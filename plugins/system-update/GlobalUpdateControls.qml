/*
 * This file is part of system-settings
 *
 * Copyright (C) 2016 Canonical Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3 as ListItems
import Lomiri.SystemSettings.Update 1.0
import "i18nd.js" as I18nd

Item {
    id: globalUpdateControls

    property bool batchMode
    property int status // A UpdateManager::Status
    property bool requireRestart: false
    property int updatesCount: 0
    property bool online: false

    property bool hidden: {
        switch (status) {
        case UpdateManager.StatusNetworkError:
        case UpdateManager.StatusServerError:
            return true;
        }
        return !online ||
               (updatesCount <= 1 && status === UpdateManager.StatusIdle)  ||
               batchMode
    }

    signal stop()
    signal requestInstall()
    signal install()

    Behavior on height {
        LomiriNumberAnimation {}
    }

    RowLayout {
        id: checking
        spacing: units.gu(2)
        anchors {
            fill: parent
            margins: units.gu(2)
        }

        Behavior on opacity {
            LomiriNumberAnimation {}
        }

        opacity: visible ? 1 : 0
        visible: {
            switch (globalUpdateControls.status) {
            case UpdateManager.StatusCheckingClickUpdates:
            case UpdateManager.StatusCheckingImageUpdates:
            case UpdateManager.StatusCheckingAllUpdates:
                return true;
            }
            return false;
        }

        ActivityIndicator {
            running: parent.visible
        }

        Label {
            objectName: "checkLabel"
            Layout.fillWidth: true
            elide: Text.ElideRight
            text: I18nd.tr("Checking for updates…")
        }

        Button {
            objectName: "updatesGlobalStopButton"
            text: I18nd.tr("Stop")
            onClicked: globalUpdateControls.stop()
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
        }
    }

    RowLayout {
        id: install
        anchors {
            fill: parent
            margins: units.gu(2)
        }
        spacing: units.gu(2)

        Behavior on opacity {
            LomiriNumberAnimation {}
        }

        opacity: visible ? 1 : 0
        visible: {
            var canInstall = globalUpdateControls.status === UpdateManager.StatusIdle;
            return canInstall && updatesCount > 1;
        }

        Label {
            objectName: "updatesGlobalInfoLabel"
            // // TRANSLATORS: %1 is number of software updates available.
            text: I18nd.tr("%1 update available",
                          "%1 updates available",
                          updatesCount).arg(updatesCount)
            Layout.fillWidth: true
        }

        Button {
            objectName: "updatesGlobalInstallButton"
            text: {
                if (globalUpdateControls.requireRestart === true) {
                    return I18nd.tr("Update all…");
                } else {
                    return I18nd.tr("Update all");
                }
            }
            onClicked: globalUpdateControls.requestInstall()
            color: theme.palette.normal.positive
        }
    }

    ListItems.ThinDivider {
        visible: !globalUpdateControls.hidden
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
    }
}
