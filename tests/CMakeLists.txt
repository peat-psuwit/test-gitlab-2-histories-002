configure_file (test_code.py.in test_code.py)
configure_file (test_push_helper.py.in test_push_helper.py)
add_test(NAME python3 COMMAND "${CMAKE_CURRENT_BINARY_DIR}/test_code.py")
add_test(NAME test_push_helper COMMAND "${CMAKE_CURRENT_BINARY_DIR}/test_push_helper.py")

add_subdirectory(utils)
add_subdirectory(mocks)

# QML tests that require graphical capabilities.
add_custom_target(qmluitests)
add_dependencies(qmluitests LomiriSettingsTest)

add_custom_target(qmltests)
add_dependencies(qmltests qmluitests)

add_subdirectory(plugins)
